#define GLM_ENABLE_EXPERIMENTAL

#include "common/Application.hpp"
#include "common/Mesh.hpp"
#include "common/ShaderProgram.hpp"

#include <iostream>
#include <vector>
#include <fstream>
#include <cmath>

using std::cout;
using std::endl;

std::vector<std::vector<float>> readGround() {
    std::vector<std::vector<float>> terrain(512, std::vector<float>(512, 0));

    std::string file_path = "./691MorkovkinData1/perlin.txt";
    std::fstream fs;
    fs.open(file_path, std::fstream::in);
    for (int i = 0; i < 512; i++) {
        for (int j = 0; j < 512; j++) {
            float value;
            fs >> value;
            cout << value << " " << value + 20 * pow(value, 3) << " " << pow((value + 40 * pow(value, 4)), 2) / 450
                 << endl;
            terrain[i][j] = value + 40 * pow(value, 4);

        }
    }
    fs.close();

    return terrain;
}


MeshPtr makeGround() {

    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;

    int size = 511;
    std::vector<std::vector<float>> values = readGround();

    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {

            glm::vec3 x1 = glm::vec3(i, j, values[i][j]);
            glm::vec3 y1(i + 1, j, values[i + 1][j]);
            glm::vec3 z1(i, j + 1, values[i][j + 1]);
            glm::vec3 x2 = glm::vec3(i + 1, j + 1, values[i + 1][j + 1]);
            glm::vec3 y2 = glm::vec3(i + 1, j, values[i + 1][j]);
            glm::vec3 z2 = glm::vec3(i, j + 1, values[i][j + 1]);

            vertices.insert(vertices.end(), {x1, y1, z1, x2, y2, z2});
            normals.insert(normals.end(),
                           {glm::normalize(x1), glm::normalize(y1), glm::normalize(z1), glm::normalize(x2),
                            glm::normalize(y2), glm::normalize(z2)});
        }
    }

    // set mesh
    DataBufferPtr bufvertices = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    bufvertices->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf_norms = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf_norms->setData(normals.size() * sizeof(float) * 3, normals.data());

    auto surface = std::make_shared<Mesh>();
    surface->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, bufvertices);
    surface->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf_norms);

    surface->setPrimitiveType(GL_TRIANGLES);
    surface->setVertexCount(vertices.size());

    surface->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.5f)));

    return surface;
}


class SampleApplication : public Application {
public:
    MeshPtr _ground;
    ShaderProgramPtr _shader;

    void makeScene() override {
        Application::makeScene();

        _ground = makeGround();
        _ground->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(-80.0f, -80.0f, -50.0f)));

        _shader = std::make_shared<ShaderProgram>("691MorkovkinData1/simple.vert", "691MorkovkinData1/simple.frag");
        _cameraMover = std::make_shared<FreeCameraMover>();
    }

    void draw() override {
        Application::draw();

        //Получаем текущие размеры экрана и выставлям вьюпорт
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Подключаем шейдер
        _shader->use();

        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        //Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку
        _shader->setMat4Uniform("modelMatrix", _ground->modelMatrix());
        _ground->draw();
    }
};

int main() {
    SampleApplication app;
    app.start();

    return 0;
}